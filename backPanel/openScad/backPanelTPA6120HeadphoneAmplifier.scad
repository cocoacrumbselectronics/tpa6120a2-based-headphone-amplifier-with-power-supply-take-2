


difference() {
    rounded_square([170, 54], [6, 6, 6, 6], center = false);
    
    // Mounting screws
    translate([6.05,  5.4]) circle(d=3,$fn=360);
    translate([169.8-6.05,  5.4]) circle(d=3,$fn=360);
    translate([6.05,  48.1]) circle(d=3,$fn=360);
    translate([169.8-6.05,  48.1]) circle(d=3,$fn=360);

    // Power
    translate([130,  (54-32)/2]) rounded_square([27, 32], [3, 3, 3, 3], center = false);
    translate([130 + 27/2 - 36/2,  27]) circle(d=3,$fn=360);
    translate([130 + 27/2 + 36/2,  27]) circle(d=3,$fn=360);

    // RCA connectors
    translate([22.75, 18]) circle(d=10.5 ,$fn=360);
    translate([22.75 + 14, 18]) circle(d=10.5 ,$fn=360);  
}



module rounded_square(dim, corners=[10,10,10,10], center=false){
  w=dim[0];
  h=dim[1];

  if (center){
    translate([-w/2, -h/2])
    rounded_square_(dim, corners=corners);
  }else{
    rounded_square_(dim, corners=corners);
  }
}

module rounded_square_(dim, corners, center=false){
  w=dim[0];
  h=dim[1];
  render(){
    difference(){
      square([w,h]);

      if (corners[0])
        square([corners[0], corners[0]]);

      if (corners[1])
        translate([w-corners[1],0])
        square([corners[1], corners[1]]);

      if (corners[2])
        translate([0,h-corners[2]])
        square([corners[2], corners[2]]);

      if (corners[3])
        translate([w-corners[3], h-corners[3]])
        square([corners[3], corners[3]]);
    }

    if (corners[0])
      translate([corners[0], corners[0]])
      intersection(){
        circle(r=corners[0],$fn=360);
        translate([-corners[0], -corners[0]])
        square([corners[0], corners[0]]);
      }

    if (corners[1])
      translate([w-corners[1], corners[1]])
      intersection(){
        circle(r=corners[1],$fn=360);
        translate([0, -corners[1]])
        square([corners[1], corners[1]]);
      }

    if (corners[2])
      translate([corners[2], h-corners[2]])
      intersection(){
        circle(r=corners[2],$fn=360);
        translate([-corners[2], 0])
        square([corners[2], corners[2]]);
      }

    if (corners[3])
      translate([w-corners[3], h-corners[3]])
      intersection(){
        circle(r=corners[3],$fn=360);
        square([corners[3], corners[3]]);
      }
  }
}